import java.awt.*;
import java.awt.event.*;
import java.io.*;

class Save{
		String str;
		Save(MainFrame mf){
		
		if(MainFrame.fileToOpen.equals("")){
			FileDialog fd=new FileDialog(mf,"File Dialog",FileDialog.SAVE);
			fd.setVisible(true);

			if(fd.getDirectory()!=null || fd.getFile()!=null){
				MainFrame.fileToOpen=fd.getFile();
				MainFrame.fileDirectory=fd.getDirectory();
			}
		}
		try{
				File f=new File(MainFrame.fileDirectory+MainFrame.fileToOpen);
				FileOutputStream fout=new FileOutputStream(f);

				PrintWriter pw=new PrintWriter(fout,true);
				if(MainFrame.fileToOpen.contains("nk")){
					// System.out.println(MainFrame.fileToOpen.contains("nk"));
					str=Encryption.encrypt(MainFrame.ta.getText());
					pw.println(str);
				}else{
					str=MainFrame.ta.getText();
					pw.println(str);
				}
				pw.close();
				fout.close();
			}catch(IOException e){
				System.out.println(e);
			}
		
	}

	
}