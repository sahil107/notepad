import java.awt.*;
import java.awt.event.*;

class FindAndReplace extends Dialog implements ActionListener
{

	TextField find,replace;
	Button b1,b2,b3,b4;
    Label l1;
    int i1,i2;
	FindAndReplace(Frame parent,String title)
	{
		super(parent,title,false);
		
		setLayout(new FlowLayout());
		setSize(500,500);
		setVisible(true);
		find=new TextField();
		b1=new Button("Find");
		b3=new Button("Find next");
		replace=new TextField();
		b2=new Button("Replace");
		b4=new Button("Replace All");
		add(find);
		add(b1);
		add(b3);
		add(replace);
		add(b2);
		add(b4);
		MainFrame.ta.requestFocus();
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});
	}
	FindAndReplace(Dialog parent, String title, String msg)
	{
		super(parent,title,false);
		setLayout(new FlowLayout());
		setSize(200,200);
		setVisible(true);
		add(new Label(msg));
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});
	}
	public void actionPerformed(ActionEvent ae)
	{
		String str=ae.getActionCommand();
		String findText=find.getText();
		String replaceText=replace.getText();
		
		
	
		if(str.equals("Find")){
				i1=MainFrame.ta.getText().indexOf(findText);
				i2=findText.length();
				if(i1>=0){
					MainFrame.ta.requestFocus();
					MainFrame.ta.select(i1,i2+i1);
				}else{
					new FindAndReplace(this,"No text","No text");
				}
				
		}
		if(str.equals("Find next")){
				i1=MainFrame.ta.getText().indexOf(findText,i1+i2);
				i2=findText.length();
				if(i1>=0){
					MainFrame.ta.requestFocus();
					MainFrame.ta.select(i1,i2+i1);
				}else{
					new FindAndReplace(this,"No text","No text");
				}

		}
		if(str.equals("Replace")){
			if(replaceText.length()>0 && findText.length()>0)
				MainFrame.ta.replaceRange(replaceText,i1,i2+i1);
			else{
				String msg="Enter some text to replace";
				new FindAndReplace(this,"Nothing to replace",msg);
			}
		}
		if(str.equals("Replace All")){
			if(replaceText.length()>0 && findText.length()>0)
				MainFrame.ta.setText(MainFrame.ta.getText().replaceAll(findText, replaceText));
			else{
				String msg="Enter some text to replace";
				new FindAndReplace(this,"Nothing to replace",msg);
			}
		}
		

	}
}
