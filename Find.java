import java.awt.*;
import java.awt.event.*;

class Find extends Dialog implements ActionListener
{

	TextField find;
	Button b1,b3;
    int i1,i2;
	Find(Frame parent,String title)
	{
		super(parent,title,false);
		
		setLayout(new FlowLayout());
		setSize(500,500);
		setVisible(true);
		find=new TextField();
		b1=new Button("Find");
		b3=new Button("Find next");
		add(find);
		add(b1);
		add(b3);
		MainFrame.ta.requestFocus();
		b1.addActionListener(this);
		b3.addActionListener(this);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});
	}
	Find(Dialog parent, String title, String msg)
	{
		super(parent,title,false);
		setLayout(new FlowLayout());
		setSize(200,200);
		setVisible(true);
		add(new Label(msg));
		// add(new Button("HEY"));
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});
	}
	public void actionPerformed(ActionEvent ae)
	{
		String str=ae.getActionCommand();
		String findText=find.getText();
		
	
		if(str.equals("Find")){
				i1=MainFrame.ta.getText().indexOf(findText);
				i2=findText.length();
				if(i1>=0){
					MainFrame.ta.requestFocus();
					MainFrame.ta.select(i1,i2+i1);
				}else{
					new Find(this,"Error","No text found!");
				}
				
		}
		if(str.equals("Find next")){
				i1=MainFrame.ta.getText().indexOf(findText,i1+i2);
				i2=findText.length();
				if(i1>=0){
					MainFrame.ta.requestFocus();
					MainFrame.ta.select(i1,i2+i1);
				}else{
					new Find(this,"Error","No text found!");
				}

		}
		

	}
}
