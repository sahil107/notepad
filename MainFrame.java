import java.awt.*;
import java.awt.event.*;

public class MainFrame extends Frame implements ActionListener,ItemListener
{
		static TextArea ta;
		CheckboxMenuItem wordWrap;
		MenuItem open,save,saveAs,exit,cut,copy,paste,find,findAndReplace;
		static String fileToOpen="";
		static String fileDirectory="";
		MainFrame(){
			super("Notepad");
			setSize(1800,1000);
			setVisible(true);
			MenuBar mbar=new MenuBar();
			setMenuBar(mbar);

			Menu fileMenu=new Menu("File");
			Menu editMenu=new Menu("Edit");
			Menu formatMenu=new Menu("Format");

			mbar.add(fileMenu);
			mbar.add(editMenu);
			mbar.add(formatMenu);
			MenuShortcut menushortcut_open = 
        	new MenuShortcut(KeyEvent.VK_O, false);
  
        	MenuShortcut menushortcut_save = 
        	new MenuShortcut(KeyEvent.VK_S, false);

        	MenuShortcut menushortcut_saveas = 
        	new MenuShortcut(KeyEvent.VK_S, true);

        	MenuShortcut menushortcut_find = 
        	new MenuShortcut(KeyEvent.VK_F, false);

        	MenuShortcut menushortcut_replace = 
        	new MenuShortcut(KeyEvent.VK_R, false);

        	MenuShortcut menushortcut_cut = 
        	new MenuShortcut(KeyEvent.VK_X, false);

        	MenuShortcut menushortcut_copy = 
        	new MenuShortcut(KeyEvent.VK_C, false);

        	MenuShortcut menushortcut_paste = 
        	new MenuShortcut(KeyEvent.VK_V, false);

        	MenuShortcut menushortcut_exit = 
        	new MenuShortcut(KeyEvent.VK_X, true);

        	MenuShortcut menushortcut_wordwrap = 
        	new MenuShortcut(KeyEvent.VK_Z, true);

        

			open=new MenuItem("Open",menushortcut_open);
		    save=new MenuItem("Save",menushortcut_save);
			saveAs=new MenuItem("Save As",menushortcut_saveas);
		    exit=new MenuItem("Exit",menushortcut_exit);
			
		    copy=new MenuItem("Copy",menushortcut_copy);
			cut=new MenuItem("Cut",menushortcut_cut);
		    paste=new MenuItem("Paste",menushortcut_paste);	
		    find=new MenuItem("Find", menushortcut_find);
			findAndReplace=new MenuItem("Find And Replace",menushortcut_replace);
			
			wordWrap=new CheckboxMenuItem("Word Wrap");

			fileMenu.add(open);
			fileMenu.add(save);
			fileMenu.add(saveAs);
			fileMenu.addSeparator();
			fileMenu.add(exit);

			editMenu.add(cut);
			editMenu.add(copy);
			editMenu.add(paste);
			editMenu.addSeparator();
			editMenu.add(find);
			editMenu.add(findAndReplace);

			formatMenu.add(wordWrap);


				ta=new TextArea();

				ta.setBounds(10,60, 1800, 1800); 
				add(ta);


			addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent we){
					System.exit(0);
				}
			});	
			open.addActionListener(this);
			save.addActionListener(this);
			find.addActionListener(this);
			findAndReplace.addActionListener(this);
			cut.addActionListener(this);
			copy.addActionListener(this);
			paste.addActionListener(this);
			exit.addActionListener(this);
			saveAs.addActionListener(this);
			wordWrap.addItemListener(this);
	}
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource()==open){
				FileDialog fd=new FileDialog(this,"File Dialog",FileDialog.LOAD);
				new Open(fd);
		}
		if(ae.getSource()==save){
				new Save(this);
		}
		if(ae.getSource()==saveAs){
				FileDialog fd=new FileDialog(this,"File Dialog",FileDialog.SAVE);
				new SaveAs(fd);
		}
		if(ae.getSource()==find){
			new Find(this,"Find");
		}
		if(ae.getSource()==findAndReplace){
			new FindAndReplace(this,"Find and replace");
		}
		if(ae.getSource()==cut){
			new Cut();
		}
		if(ae.getSource()==copy){
			new Copy(); 
		}
		if(ae.getSource()==paste){
			new Paste(this);
		}
		 if(ae.getSource()==exit){
		 	System.exit(0);
		}

	}
	public void itemStateChanged(ItemEvent ie){
		if(ie.getSource()==wordWrap){
			if(wordWrap.getState()){
				String content=ta.getText();
				this.remove(ta);
				ta=new TextArea(content,0,0,TextArea.SCROLLBARS_VERTICAL_ONLY);
				this.add(ta);
				ta.setBounds(10,60,1800,1800);
				this.revalidate();
			}else{
				String content=ta.getText();
				this.remove(ta);
				ta=new TextArea(content,0,0,TextArea.SCROLLBARS_BOTH);
				this.add(ta);
				ta.setBounds(10,60,1800,1800);
				this.revalidate();
			}
		}
	}
	public static void main(String[] args) {
		new MainFrame();
	}
	
}
